<?php
function tentukan_nilai($number)
{
    if (($number >= 85) && ($number <= 100)){
      return "Sangat Baik<br>";
    }
    else if (($number >= 70) && ($number < 85)){
      return "Baik<br>";
    }
    else if (($number >= 60) && ($number < 70)){
      return "Cukup<br>";
    }
    else{
      return "Kurang<br>";
    }
}

//TEST CASES
echo "Nilai 98: ... ".tentukan_nilai(98); //Sangat Baik
echo "Nilai 76: ... ".tentukan_nilai(76); //Baik
echo "Nilai 67: ... ".tentukan_nilai(67); //Cukup
echo "Nilai 43: ... ".tentukan_nilai(43); //Kurang
?>
