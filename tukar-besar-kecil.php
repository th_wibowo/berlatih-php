<?php
// CARA PERTAMA dengan menggunakan nilai ASCII
/*function tukar_besar_kecil($string){
  $newString = "";
  $char = "";
  for ($i = 0; $i < strlen($string); $i++){
    $ascii = ord($string[$i]);
    if(($ascii >= 65) && ($ascii <= 90)){
      $position = $ascii - 65;
      $char = chr($position + 97);
    }
    else if(($ascii >= 97) && ($ascii <= 122)){
      $position = $ascii - 97;
      $char = chr($position + 65);
    }
    else{
      $char = $string[$i];
    }
    $newString .= $char;
  }
  return $newString."<br>";
}*/

// CARA KEDUA dengan menggunakan fungsi ctype_upper(), ctype_lower(), strtoupper(), strtolower()
function tukar_besar_kecil($string){
  $newString = "";
  $char = "";
  for ($i = 0; $i < strlen($string); $i++){
    if(ctype_upper($string[$i])){
      $char = strtolower($string[$i]);
    }
    else if(ctype_lower($string[$i])){
      $char = strtoupper($string[$i]);
    }
    else{
      $char = $string[$i];
    }
    $newString .= $char;
  }
  return $newString."<br>";
}
// CARA KETIGA dengan membuat array berisi himpunan alfabet kapital dan non kapital
/*function tukar_besar_kecil($string){
  $alphabet = array(
    array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"),
    array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z")
  );
  $newString = "";
  for ($i = 0; $i < strlen($string); $i++){
    // Jika huruf dalam $string termasuk huruf kapital
    if(ctype_upper($string[$i])){
      for ($j = 0; $j < 26; $j++){
        if($string[$i] == $alphabet[0][$j]){
          $newString .= $alphabet[1][$j];
          break;
        }
      }
    }
    // Jika bukan huruf kapital
    else if(ctype_lower($string[$i])){
      for ($j = 0; $j < 26; $j++){
        if($string[$i] == $alphabet[1][$j]){
          $newString .= $alphabet[0][$j];
          break;
        }
      }
    }
    else{
      $newString .= $string[$i];
    }
  }
  return $newString."<br>";
}*/
// TEST CASES
echo "\"<b>Hello World</b>\" menjadi ===> <b>".tukar_besar_kecil('Hello World')."</b>"; // "hELLO wORLD"
echo "\"<b>I aM aLAY</b>\" menjadi ===> <b>".tukar_besar_kecil('I aM aLAY')."</b>"; // "i Am Alay"
echo "\"<b>My Name is Bond!!</b>\" menjadi ===> <b>".tukar_besar_kecil('My Name is Bond!!')."</b>"; // "mY nAME IS bOND!!"
echo "\"<b>IT sHOULD bE me</b>\" menjadi ===> <b>".tukar_besar_kecil('IT sHOULD bE me')."</b>"; // "it Should Be ME"
echo "\"<b>001-A-3-5TrdYW</b>\" menjadi ===> <b>".tukar_besar_kecil('001-A-3-5TrdYW')."</b>"; // "001-a-3-5tRDyw"

?>
