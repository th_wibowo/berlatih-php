<?php
// CARA PERTAMA dengan membuat sebuah array berisi alfabet kapital dan non kapital
/*function ubah_huruf($string){
  $alphabet = array(
    array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"),
    array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z")
  );
  $newString = "";
  for ($i = 0; $i < strlen($string); $i++){
    // Jika huruf dalam $string termasuk huruf kapital
    if(ctype_upper($string[$i])){
      for ($j = 0; $j < 26; $j++){ //mengecek kesamaan huruf dengan elemen $alphabet[0][], dan mengambil elemen setelahnya sebagai isi dari string yg baru
        if($string[$i] == $alphabet[0][$j]){
          if($j == 25){
            $newString .= $alphabet[0][0];
          }
          else{
            $newString .= $alphabet[0][$j+1];
          }
          break;
        }
      }
    }
    // Jika bukan huruf kapital
    else{
      for ($j = 0; $j < 26; $j++){  //mengecek kesamaan huruf dengan elemen $alphabet[1][]
        if($string[$i] == $alphabet[1][$j]){
          if($j == 25){
            $newString .= $alphabet[1][0];
          }
          else{
            $newString .= $alphabet[1][$j+1];
          }
          break;
        }
      }
    }
  }
  return $newString."<br>";
}*/
// CARA KEDUA dengan menggunakan nilai ASCII
function ubah_huruf($string){
  $newString = "";
  $char = "";
  for ($i = 0; $i < strlen($string); $i++){
    $ascii = ord($string[$i]);
    if((($ascii >= 97) && ($ascii < 122)) || (($ascii >= 65) && ($ascii < 90))){
      $char = chr($ascii + 1);
    }
    else if($ascii == 122){
      $char = chr(97);
    }
    else if($ascii == 90){
      $char = chr(65);
    }
    $newString .= $char;
  }
  return $newString."<br>";
}

// TEST CASES
echo "\"<b>wow</b>\" diubah menjadi ===> <b>".ubah_huruf('wow')."</b>"; // xpx
echo "\"<b>developer</b>\" diubah menjadi ===> <b>".ubah_huruf('developer')."</b>"; // efwfmpqfs
echo "\"<b>laravel</b>\" diubah menjadi ===> <b>".ubah_huruf('laravel')."</b>"; // mbsbwfm
echo "\"<b>keren</b>\" diubah menjadi ===> <b>".ubah_huruf('keren')."</b>"; // lfsfo
echo "\"<b>semangat</b>\" diubah menjadi ===> <b>".ubah_huruf('semangat')."</b>"; // tfnbohbu
//BONUS TEST CASES
echo "<===== BONUS =====><br>";
echo "\"<b>HmunjdQ</b>\" diubah menjadi ===> <b>".ubah_huruf('HmunjdQ')."</b>"; // ApSp
?>
